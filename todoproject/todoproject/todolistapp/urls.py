from django.urls import path
from todolistapp import views
urlpatterns = [
    path("", views.index, name="home"),
    path("add", views.add_task, name="add-task"),
    path("delete", views.delete_task, name="delete-task"),
    path("edit", views.edit_task, name="edit-task"),
    ]
