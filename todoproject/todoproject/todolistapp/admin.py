from django.contrib import admin
from .models import UserTasks
# Register your models here.

class UserTasksAdmin(admin.ModelAdmin):
    list_display = ['id','UserId','Title','Description']
admin.site.register(UserTasks, UserTasksAdmin)
