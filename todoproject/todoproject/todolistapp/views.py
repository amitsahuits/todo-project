from django.shortcuts import render, redirect
from . import forms
from .models import UserTasks
from django.http import JsonResponse,HttpResponse
import json
# from asgiref.sync import sync_to_async
# import async.io
# from config.settings.base import AUTH_USER_MODEL
from django.contrib.auth import get_user_model
User = get_user_model()

# Create your views here.

def index(request):
    if '_auth_user_id' not in request.session :
        return render(request, 'pages/home.html',{'user':False})
    else:
        tasks = UserTasks.objects.filter(UserId = request.session['_auth_user_id']).order_by('-id')
        data_dict = {
            'user': True,
            'tasks': tasks
        }
  
        return render(request, 'pages/home.html',context=data_dict)


def add_task(request):
    if '_auth_user_id' in request.session :
        if request.method == 'POST':
            user_id = request.session['_auth_user_id']
            title = request.POST['title']
            desc = request.POST['desc']
            new_task = UserTasks(UserId= user_id, Title = title, Description = desc)
            new_task.save()

            # send EMAIL
    return redirect('/')

def delete_task(request):
    if '_auth_user_id' in request.session :
        if request.method == 'POST':
            user_id = request.session['_auth_user_id']
            task_id = request.POST['task_id']
            UserTasks(id=task_id, UserId=user_id).delete()
            json_data = json.dumps('success')
            return HttpResponse(json_data, content_type="application/json")
    else:
        return redirect('/')

def edit_task(request):
    if '_auth_user_id' in request.session :
        if request.method == 'POST':
            user_id = request.session['_auth_user_id']
            task_id = request.POST['task_id']
            title = request.POST['title']
            desc = request.POST['desc']
            new_task = UserTasks(id=task_id, UserId=user_id)
            user_task.Title = Title
            user_task.Description = desc
            json_data = json.dumps('success')
            return HttpResponse(json_data, content_type="application/json")
    else:
        return redirect('/')
