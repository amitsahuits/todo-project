from django import forms
from .models import UserTasks

class TaskForm(forms.ModelForm):
    class Meta:
        model = UserTasks
        #fields = ('Title','Description')
        #exclude = ['UserId']
        fields = '__all__'
